import java.net.URL;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.InetAddress;
import java.net.UnknownHostException;
/**
 * Práctica nº7 Usando la clase URL
 * Esta clase cuenta con método main, constructor y el método leerURL
 * @author José Antonio
 * @version 14/02/2022
 */
public class PracticaURL {
    
    URL url = null;
    URLConnection urlCon = null;

    //CONSTRUCTOR -----------------------------------------------------
    /**
     * Se debe crear objeto URL que represente la dirección al siguiente recurso de Internet.
     */
    public PracticaURL() {
        try {
            this.url = new URL("https://wordpress.org/plugins/readme.txt");
            try {
                this.urlCon = url.openConnection();
            } catch (Exception e) {
                //TODO: handle exception
            }
        } catch (MalformedURLException mex) {
            //TODO: handle exception
            System.err.println("Error URL: " + mex.getMessage());
            
        }
        
        //usar métodos de la clase para: protocolo URL, ruta completa fichero, nombre del host
        System.out.println("\tNombre del Host: " + url.getHost());
        System.out.println("\tProtocolo: " + url.getProtocol());
        System.out.println("\tRuta completa: " + url.toString());
        // System.out.println("Nombre del host: " + url.getHostName());
        
        try {
            // objeto de la clase InputStream
            InputStream inputStream = urlCon.getInputStream();
            // flujo de datos de entrada desde URL
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            // llamada a método leerURL
            leerURL(inputStream);
        } catch (IOException ioe) {
            //TODO: handle exception
            ioe.printStackTrace();
        }
        
    }

    //MAIN ------------------------------------------------------------
    /**
     * main
     * @param args
     */
    public static void main(String[] args) {
        new PracticaURL();
    }

    //método leerURL --------------------------------------------------
    /**
     * método recibe como parámetro objeto de tipo InputStream
     * leer el flujo de datos de entrada
     * 
     * @param _inputStream
     */
    public void leerURL(InputStream _inputStream) {
        BufferedReader lector;
        lector = new BufferedReader(new InputStreamReader(_inputStream));
        String lineaLeida;

        try {
            while((lineaLeida = lector.readLine()) != null) {
                System.out.println(lineaLeida);
            }
        } catch (IOException ex) {
            //TODO: handle exception
            System.err.println("Error al leer flujo de entrada: " + ex);
        }

        try {
            lector.close();
        } catch (IOException e) {
            //TODO: handle exception
            System.err.println("Error al cerrar flujo de entrada: " + e);
        }
    }
}
