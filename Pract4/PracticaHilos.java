public class PracticaHilos {
    
    
    /** 
     * @param args
     */
    //----------------------------------------------------- main
    public static void main(String[] args) {
        
        PracticaHilos ph = new PracticaHilos();
        ph.pruebaEjecucionNormal();
        ph.pruebaMultiHilo();
    }
    //----------------------------------------------------- método 1
    public void pruebaEjecucionNormal() {
        System.out.println("----------------------------- Prueba Normal");
        for(byte i=1;i<=5;i++){
            for (byte j=1;j<=5;j++){
                System.out.println(i+"] Contador prueba normal "+j);
            }
        }
    }
    //----------------------------------------------------- método 2
    public void pruebaMultiHilo() {
        System.out.println("----------------------------- Prueba multihilo");
        Hilo hilo1 = new Hilo("Hilo 1");
        Hilo hilo2 = new Hilo("Hilo 2");
        hilo1.start();
        hilo2.start();
    }
    //-----------------------------------------------------
}
