import javax.swing.JLabel;

/**
 * Práctica nº6 programación de hilos
 * @author José Antonio
 * @version 6/2/2022
/ */

public class HiloReloj extends Thread{
    
    //------------------------------------------------------ 
    private int segundos;
    private long retrasoEnInicio;
    private JLabel label1;
    //------------------------------------------------------ constructor
    /**
     * 
     * @param nombre. Indica el nombre asignado al hilo
     * @param retrasoEnInicio. Indica el tiempo en ms asignado.
     * @param label. Indica el componente JLabel
     */
    public HiloReloj(String nombre,long retrasoEnInicio, JLabel label){
        super(nombre);
        this.retrasoEnInicio = retrasoEnInicio;
        this.label1 = label;
        this.segundos = 0;
    }
    //------------------------------------------------------ run()
    /**
     * Método run() sobreescrito pertenece a la calse Thread
     * @throws InterruptedException es obligatorio utilizar con el método estático sleep()
     */
    @Override
    public void run() {
        
        try {
            Thread.sleep(retrasoEnInicio);
            while(true){
                segundos++;
                //actualizar el JLabel mostrando mensaje (nombre-hilo x segundo)
                label1.setText("R: "+segundos);
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            //TODO: handle exception
            System.out.println("----- Interrumpido el Hilo: " + getName());
            return;
        }
    }
}
