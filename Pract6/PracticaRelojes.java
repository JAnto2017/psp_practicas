import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;

/**
* @author: José Antonio
* @version: 6/2/2022
*/
public class PracticaRelojes implements Runnable{
    JFrame ventana;
    JLabel etiqueta1;
    JLabel etiqueta2;
    JButton boton;
    JPanel panelContenido;
    //------------------------------------------------------------------------- constructor
    /**
     * Permite la creación del panel con sus compoentes
     */
    public PracticaRelojes() {
        //preparamos la ventana------------------------------------------------
        ventana = new JFrame("PrActica n-6");
        ventana.setSize(300,300);
        ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //creamos los componentes----------------------------------------------
        etiqueta1 = new JLabel();
        etiqueta2 = new JLabel();
        boton = new JButton("Parar Procesos");
        //crea el contenedor---------------------------------------------------
        panelContenido = new JPanel();
        panelContenido.setLayout(null);
        //asocia componentes al contenedor-------------------------------------
        etiqueta1.setText("Reloj-1");
        etiqueta1.setBounds(94, 21, 110, 20);
        panelContenido.add(etiqueta1);
        etiqueta2.setBounds(94, 85, 110, 20);
        etiqueta2.setText("Reloj-2");
        panelContenido.add(etiqueta2);
        boton.setBounds(94,198,75,25);
        panelContenido.add(boton);
        //asocia contenedor a ventana-------------------------------------------
        ventana.setContentPane(panelContenido);
        //hacer visible la ventana----------------------------------------------
        ventana.setVisible(true);
        //instancias de la clase HiloReloj--------------------------------------
        HiloReloj hr1 = new HiloReloj("R-1", 0, etiqueta1);
        hr1.start();
        HiloReloj hr2 = new HiloReloj("R-2", 3000, etiqueta2);
        hr2.start();
        //evento del button-----------------------------------------------------
        boton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    hr1.interrupt();
                    hr2.interrupt();
                    run(); // no es necesario s/guión de la práctica. Se añade para probar
                    System.exit(0);
                } catch (SecurityException ex) {
                    //TODO: handle exception
                    ex.printStackTrace();
                }
                
            }
        });
    }
        
    
    // ---------------------------------------------------------------------------- main
    /** 
     * método principal main() punto de entrada del programa Java
     * @param args
     */
    public static void main(String[] args) {
        new PracticaRelojes();
        //new HiloReloj().start();
    }
    //------------------------------------------------------------------------------ 
    /**
     * Este método se añada extra únicametne para establecer un tiempo de cierra de la ventana al pulsar el button
     */
    @Override
    public void run() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            System.err.println("Excepción producida");
        }
    }
}
