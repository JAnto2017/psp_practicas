public class PracticaCarreraHilos {
    
    
    /** 
     * @param args
     */
    public static void main(String[] args) {
        HiloCarrera hc1 = new HiloCarrera("--- Hilo A ---");
        HiloCarrera hc2 = new HiloCarrera("--- Hilo B ---");
        HiloCarrera hc3 = new HiloCarrera("--- Hilo C ---");

        hc1.start();
        hc2.start();
        hc3.start();
    }
}
