public class HiloCarrera extends Thread{
    
    int metrosRecorridos, distanciaMeta;
    //------------------------------------------ constructor
    public HiloCarrera(String nombreHilo) {
        super(nombreHilo);
        metrosRecorridos = 0;
        distanciaMeta = 100;
    }
    //------------------------------------------ run()
    @Override
    public void run() {
        while (metrosRecorridos <= distanciaMeta){
            metrosRecorridos++;
        }
        System.out.println(this.getName()+" ha llegado a la meta");
    }
}
