import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Crea un nuevo servidor escucha peticiones de clientes
 * @author José Antonio
 * @version 19/02/2022
 */
public class Servidor {
    
    private final int puertoServidor = 6000;
    private ServerSocket serverSocket;
    private Socket socketCliente;
    //BufferedReader lectorFlujoEntrada;

    //--------------------------------------------------------------------------------- CONSTRUCTOR
    public Servidor() {
        System.out.println("==================== Ejecutando servidor ====================");

        try {
            //1. crear nuevo socket para servidor. Clase ServerSocket
            serverSocket = new ServerSocket(puertoServidor);
            
            //2. crear nuevo socket para cliente. Usar constructor sin parámetros de la clase Socket
            socketCliente = serverSocket.accept();

            System.out.println("Servidor creado,empezamos a esperar clientes");

            //3. debemos hacer nuestro servidor se ponga a la espera cliente le haga petición
            
            
            System.out.println("Cliente conectado desde "+socketCliente.getInetAddress().getHostAddress()+":"+socketCliente.getPort());

            //podemos obtener un flujo de entrada para recibir el mensaje desde el cliente
            // 4. obtenemos el lector de flujo de entrada para leer mensajes que envía el cliente

            InputStream isDeCliente = socketCliente.getInputStream();
            //OutputStream osACliente = socketCliente.getOutputStream();
            InputStreamReader isrDeCliente = new InputStreamReader(isDeCliente, "UTF-8");
            BufferedReader lectorFlujoEntrada = new BufferedReader(isrDeCliente);

            String mensajeRecibido = lectorFlujoEntrada.readLine();
            System.out.println(mensajeRecibido);

            //5.cerramos el socket del servidor
            socketCliente.close();
            serverSocket.close();

            System.out.println("La conexión ha terminado");

        } catch (IOException e) {
            //TODO: handle exception
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }
    //----------------------------------------------------------------------------------- 
}
