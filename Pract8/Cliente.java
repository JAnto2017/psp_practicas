import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Cliente {
    
    private final String direccionServidor = "localhost";
    private final int puertoConocidoServidor = 6000;
    private Socket socketConexion;
    private DataOutputStream flujoSalida;

    public Cliente() {
        
        System.out.println("==================== Ejecutando cliente ====================");

        try {
            //1. instanciamos nuestro socket para conexión al servidor
            socketConexion = new Socket();
            InetSocketAddress direccion = new InetSocketAddress(direccionServidor, puertoConocidoServidor);
            socketConexion.connect(direccion);

            //2. obtenemos el escritor
            InputStream isDeServidor = socketConexion.getInputStream();
            OutputStream osAServidor = socketConexion.getOutputStream();

            //3. una vez que tenemos el escritor de flujo de salida, podemos escribir un mensaje
            String mensajeParaElServidor = "Este es el mensaje de saludo desde cliente";
            osAServidor.write(mensajeParaElServidor.getBytes());

            //4. cerramos el socket que hemos usado
            System.out.println("Socket cliente cerrado");
            socketConexion.close();

        } catch (IOException e) {
            //TODO: handle exception
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }

    //------------------------------------------------------------------

    private DataOutputStream obtenerFlujoSalida(Socket socket) {
        try {
            return new DataOutputStream(socket.getOutputStream());
        } catch (IOException ex) {
            //TODO: handle exception
            System.err.println(ex.getMessage());
        }
        return null;
    }
}
