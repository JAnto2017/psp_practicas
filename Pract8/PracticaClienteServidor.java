import javax.swing.WindowConstants;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * Muesta interfaz de usuario e instancia objetos de las clases Servidor y Cliente
 * @author José Antonio
 * @version 19/02/2022
 */
public class PracticaClienteServidor extends JFrame{

    private JButton bIniciar = new JButton();
    private JLabel lTexto = new JLabel();

    /**
     * Constructor---------------------------------------------------------------------------------
     */
    public PracticaClienteServidor() {
        super();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        int frameWidth = 300;
        int frameHeight = 300;
        setSize(frameWidth,frameHeight);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (d.width - getSize().width) / 2;
        int y = (d.height - getSize().height) / 2;
        setLocation(x,y);
        setTitle("PRACTICA 8");
        setResizable(false);
        Container cp = getContentPane();
        cp.setLayout(null);
        bIniciar.setBounds(104,144,75,25);
        bIniciar.setText("Iniciar");
        bIniciar.setMargin(new Insets(2,2,2,2));
        bIniciar.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt) {
                bIniciar_ActionPerformed(evt);
            }
        });
        cp.add(bIniciar);
        lTexto.setBounds(53,16,183,20);
        lTexto.setText("PROGRAMA CLIENTE SERVIDOR");
        cp.add(lTexto);
        setVisible(true);
    }

    /**
     * Main ---------------------------------------------------------------------------------------
     * @param args
     */
    public static void main(String[] args) {
        new PracticaClienteServidor();

        //nueva instancia de Servidor
        Servidor servidor = new Servidor();
    }

    /**
     * método para el evento del botón onclick -----------------------------------------------------
     * @param evt
     */
    public void bIniciar_ActionPerformed(ActionEvent evt) {
        Cliente cliente = new Cliente();
    }
}
