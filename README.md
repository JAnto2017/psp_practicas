# MÓDULO PSP DEL CFGS MULTIPLATAFORMA

## _Serie de prácticas realizadas en el módulo profesional PSP perteneciente al CGFS Multiplataforma_

## P1 -> Ejecutar procesos desde Java
## P2 -> Ejecutando programas de Java desde Java (Parte I)
## P2 -> Comunicando a través de un flujo de datos (Parte II)
## P3 -> Planificación Round-Robin
## P4 -> Programación multi-hilo
## P5 -> Carrera de hilos
## P6 -> Relojes
## P7 -> Usando la clase URL
## P8 -> Programa cliente - servidor
